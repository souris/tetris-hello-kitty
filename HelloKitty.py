import pygame as pg
import random
import sys
import time

fps = 25
window_w, window_h = 1080, 720
block, cup_h, cup_w = 20, 20, 10

side_freq, down_freq = 0.15, 0.1  # передвижение в сторону и вниз

side_margin = int((window_w - cup_w * block) / 2)
top_margin = window_h - (cup_h * block) - 100

colors = ((255, 37, 72), (219, 64, 255),(255,50,131), (255, 94, 196),
          (143, 50, 224), (255,0,161))  # цвета блоков(яркие)
lightcolors = ((251, 109, 132), (228, 111, 255),(255,101,162),
               (255, 138, 212), (161, 95, 219),(251,92,192))  # светлые цвета
brd_color = (255, 34, 34)
bg_color = (255, 255, 255)
txt_color = (255, 34, 34)
title_color = (117, 0, 92)
info_color = (188, 66, 138)
pause_color = (212, 0, 166, 127)

fig_w, fig_h = 5, 5
empty = '0'

figures = {'S': [['00000',
                  '00000',
                  '00xx0',
                  '0xx00',
                  '00000'],
                 ['00000',
                  '00x00',
                  '00xx0',
                  '000x0',
                  '00000']],
           'Z': [['00000',
                  '00000',
                  '0xx00',
                  '00xx0',
                  '00000'],
                 ['00000',
                  '00x00',
                  '0xx00',
                  '0x000',
                  '00000']],
           'J': [['00000',
                  '0x000',
                  '0xxx0',
                  '00000',
                  '00000'],
                 ['00000',
                  '00xx0',
                  '00x00',
                  '00x00',
                  '00000'],
                 ['00000',
                  '00000',
                  '0xxx0',
                  '000x0',
                  '00000'],
                 ['00000',
                  '00x00',
                  '00x00',
                  '0xx00',
                  '00000']],
           'L': [['00000',
                  '000x0',
                  '0xxx0',
                  '00000',
                  '00000'],
                 ['00000',
                  '00x00',
                  '00x00',
                  '00xx0',
                  '00000'],
                 ['00000',
                  '00000',
                  '0xxx0',
                  '0x000',
                  '00000'],
                 ['00000',
                  '0xx00',
                  '00x00',
                  '00x00',
                  '00000']],
           'I': [['00x00',
                  '00x00',
                  '00x00',
                  '00x00',
                  '00000'],
                 ['00000',
                  '00000',
                  'xxxx0',
                  '00000',
                  '00000']],
           'O': [['00000',
                  '00000',
                  '0xx00',
                  '0xx00',
                  '00000']],
           'T': [['00000',
                  '00x00',
                  '0xxx0',
                  '00000',
                  '00000'],
                 ['00000',
                  '00x00',
                  '00xx0',
                  '00x00',
                  '00000'],
                 ['00000',
                  '00000',
                  '0xxx0',
                  '00x00',
                  '00000'],
                 ['00000',
                  '00x00',
                  '0xx00',
                  '00x00',
                  '00000']]}


def pauseScreen():
    pause = pg.Surface((window_w, window_h), pg.SRCALPHA)
    pause.fill(pause_color)
    display_surf.blit(pause, (0, 0))


def main():
    global fps_clock, display_surf, basic_font, big_font, line_clear, fig_down
    pg.init()
    pg.mixer.init()
    fps_clock = pg.time.Clock()
    display_surf = pg.display.set_mode((window_w, window_h))
    basic_font = pg.font.SysFont('F77 Minecraft', 42)
    big_font = pg.font.SysFont('F77 Minecraft', 60)
    pg.display.set_caption('HELLO KITTY TETRIS')
    showText('HELLO KITTY TETRIS')
    PlsyMusic('TetrisTheme.wav')
    line_clear = pg.mixer.Sound("LineClear.wav")
    game_over = pg.mixer.Sound("GameOver.wav")
    while True:  # начало игры
        runTetris()
        pauseScreen()
        game_over.play()
        showText('GAME OVER')


def runTetris():
    cup = emptycup()
    last_move_down = time.time()
    last_side_move = time.time()
    last_fall = time.time()
    going_down = False
    going_left = False
    going_right = False
    points = 0
    level, fall_speed = calcSpeed(points)
    fallingFig = getNewFig()
    nextFig = getNewFig()

    while True:
        if fallingFig is None:
            # если нет падающих фигур, генерируем новую
            fallingFig = nextFig
            nextFig = getNewFig()
            last_fall = time.time()

            if not checkPos(cup, fallingFig):
                return  
        quitGame()
        for event in pg.event.get():
            if event.type == pg.KEYUP:
                if event.key == pg.K_SPACE:
                    pauseScreen()
                    pg.mixer.music.pause()
                    showText('Пауза')
                    last_fall = time.time()
                    last_move_down = time.time()
                    last_side_move = time.time()
                elif event.key == pg.K_LEFT:
                    going_left = False
                elif event.key == pg.K_RIGHT:
                    going_right = False
                elif event.key == pg.K_DOWN:
                    going_down = False

            elif event.type == pg.KEYDOWN:
                # перемещение фигуры вправо и влево
                if event.key == pg.K_LEFT and checkPos(
                        cup, fallingFig, adjX=-1):
                    fallingFig['x'] -= 1
                    going_left = True
                    going_right = False
                    last_side_move = time.time()

                elif event.key == pg.K_RIGHT and checkPos(cup, fallingFig, adjX=1):
                    fallingFig['x'] += 1
                    going_right = True
                    going_left = False
                    last_side_move = time.time()

                # поворачиваем фигуру, если есть место
                elif event.key == pg.K_UP:
                    fallingFig['rotation'] = (
                        fallingFig['rotation'] + 1) % len(figures[fallingFig['shape']])
                    if not checkPos(cup, fallingFig):
                        fallingFig['rotation'] = (
                            fallingFig['rotation'] - 1) % len(figures[fallingFig['shape']])

                # ускоряем падение фигуры
                elif event.key == pg.K_DOWN:
                    going_down = True
                    if checkPos(cup, fallingFig, adjY=1):
                        fallingFig['y'] += 1
                    last_move_down = time.time()

                # мгновенный сброс вниз
                elif event.key == pg.K_RETURN:
                    going_down = False
                    going_left = False
                    going_right = False
                    for i in range(1, cup_h):
                        if not checkPos(cup, fallingFig, adjY=i):
                            break
                    fallingFig['y'] += i - 1

        # управление падением фигуры при удержании клавиш
        if (going_left or going_right) and time.time() - \
                last_side_move > side_freq:
            if going_left and checkPos(cup, fallingFig, adjX=-1):
                fallingFig['x'] -= 1
            elif going_right and checkPos(cup, fallingFig, adjX=1):
                fallingFig['x'] += 1
            last_side_move = time.time()

        if going_down and time.time() - last_move_down > down_freq and checkPos(cup,
                                                                                fallingFig, adjY=1):
            fallingFig['y'] += 1
            last_move_down = time.time()

        if time.time() - last_fall > fall_speed:  # свободное падение фигуры
            if not checkPos(
                    cup,
                    fallingFig,
                    adjY=1):  # проверка "приземления" фигуры
                # фигура приземлилась, добавляем ее в содержимое стакана
                addToCup(cup, fallingFig)
                points += clearCompleted(cup)
                level, fall_speed = calcSpeed(points)
                fallingFig = None
            else:  # фигура пока не приземлилась, продолжаем движение вниз
                fallingFig['y'] += 1
                last_fall = time.time()

        # рисуем окно игры со всеми надписями
        display_surf.fill(bg_color)
        drawTitle()
        pg.mixer.music.unpause()
        gamecup(cup)
        drawInfo(points, level)
        drawnextFig(nextFig)
        if fallingFig is not None:
            drawFig(fallingFig)
        pg.display.update()
        fps_clock.tick(fps)


def txtObjects(text, font, color):
    surf = font.render(text, True, color)
    return surf, surf.get_rect()


def PlsyMusic(sound):
    pg.mixer.music.load(sound)
    pg.mixer.music.set_volume(0.5)
    pg.mixer.music.play(-1)


def stopGame():
    pg.quit()
    sys.exit()


def checkKeys():
    quitGame()

    for event in pg.event.get([pg.KEYDOWN, pg.KEYUP]):
        if event.type == pg.KEYDOWN:
            continue
        return event.key
    return None


def showText(text):
    titleSurf, titleRect = txtObjects(text, big_font, title_color)
    titleRect.center = (int(window_w / 2), int(window_h / 2) - 3)
    display_surf.blit(titleSurf, titleRect)

    pressKeySurf, pressKeyRect = txtObjects(
        'Нажмите любую клавишу для начала игры', basic_font, title_color)
    pressKeyRect.center = (int(window_w / 2), int(window_h / 2) + 100)
    display_surf.blit(pressKeySurf, pressKeyRect)

    while checkKeys() is None:
        pg.display.update()
        fps_clock.tick()


def quitGame():
    for event in pg.event.get(
            pg.QUIT):  # проверка всех событий, приводящих к выходу из игры
        stopGame()
    for event in pg.event.get(pg.KEYUP):
        if event.key == pg.K_ESCAPE:
            stopGame()
        pg.event.post(event)


def calcSpeed(points):
    # вычисляет уровень
    level = int(points / 10) + 1
    fall_speed = 0.27 - (level * 0.02)
    return level, fall_speed


def getNewFig():
    # возвращает новую фигуру со случайным цветом и углом поворота
    shape = random.choice(list(figures.keys()))
    newFigure = {'shape': shape,
                 'rotation': random.randint(0, len(figures[shape]) - 1),
                 'x': int(cup_w / 2) - int(fig_w / 2),
                 'y': -2,
                 'color': random.randint(0, len(colors) - 1)}
    return newFigure


def addToCup(cup, fig):
    for x in range(fig_w):
        for y in range(fig_h):
            if figures[fig['shape']][fig['rotation']][y][x] != empty:
                cup[x + fig['x']][y + fig['y']] = fig['color']


def emptycup():
    # создает пустой стакан
    cup = []
    for i in range(cup_w):
        cup.append([empty] * cup_h)
    return cup


def incup(x, y):
    return x >= 0 and x < cup_w and y < cup_h


def checkPos(cup, fig, adjX=0, adjY=0):
    # проверяет, находится ли фигура в границах стакана, не сталкиваясь с
    # другими фигурами
    for x in range(fig_w):
        for y in range(fig_h):
            abovecup = y + fig['y'] + adjY < 0
            if abovecup or figures[fig['shape']
                                   ][fig['rotation']][y][x] == empty:
                continue
            if not incup(x + fig['x'] + adjX, y + fig['y'] + adjY):
                return False
            if cup[x + fig['x'] + adjX][y + fig['y'] + adjY] != empty:
                return False
    return True


def isCompleted(cup, y):
    # проверяем наличие полностью заполненных рядов
    for x in range(cup_w):
        if cup[x][y] == empty:
            return False
    return True


def clearCompleted(cup):
    # Удаление заполенных рядов и сдвиг верхних рядов вниз
    removed_lines = 0
    y = cup_h - 1
    while y >= 0:
        if isCompleted(cup, y):
            line_clear.play()
            for pushDownY in range(y, 0, -1):
                for x in range(cup_w):
                    cup[x][pushDownY] = cup[x][pushDownY - 1]
            for x in range(cup_w):
                cup[x][0] = empty
            removed_lines += 1
        else:
            y -= 1
    return removed_lines


def convertCoords(block_x, block_y):
    return (side_margin + (block_x * block)), (top_margin + (block_y * block))


def drawBlock(block_x, block_y, color, pixelx=None, pixely=None):
    # отрисовка квадратных блоков, из которых состоят фигуры
    if color == empty:
        return
    if pixelx is None and pixely is None:
        pixelx, pixely = convertCoords(block_x, block_y)
    pg.draw.rect(
        display_surf,
        colors[color],
        (pixelx + 1,
         pixely + 1,
         block - 1,
         block - 1),
        0,
        3)
    pg.draw.rect(
        display_surf,
        lightcolors[color],
        (pixelx + 1,
         pixely + 1,
         block - 4,
         block - 4),
        0,
        3)


def gamecup(cup):
    # граница игрового поля-стакана
    pg.draw.rect(
        display_surf,
        brd_color,
        (side_margin - 4,
         top_margin - 4,
         (cup_w * block) + 8,
         (cup_h * block) + 8),
        5)

    # фон игрового поля
    pg.draw.rect(
        display_surf,
        bg_color,
        (side_margin,
         top_margin,
         block * cup_w,
         block * cup_h))
    for x in range(cup_w):
        for y in range(cup_h):
            drawBlock(x, y, cup[x][y])


def drawTitle():
    titleSurf = big_font.render('HELLO KITTY TETRIS', True, title_color)
    titleRect = titleSurf.get_rect()
    titleRect.center = (int(window_w / 2), int(window_h / 2) - 300)
    display_surf.blit(titleSurf, titleRect)


def drawInfo(points, level):
    pointsSurf = basic_font.render(f'Баллы: {points}', True, txt_color)
    pointsRect = pointsSurf.get_rect()
    pointsRect.topleft = (window_w - 1000, 180)
    display_surf.blit(pointsSurf, pointsRect)

    levelSurf = basic_font.render(f'Уровень: {level}', True, txt_color)
    levelRect = levelSurf.get_rect()
    levelRect.topleft = (window_w - 1000, 250)
    display_surf.blit(levelSurf, levelRect)

    pausebSurf = basic_font.render('Пауза: пробел', True, info_color)
    pausebRect = pausebSurf.get_rect()
    pausebRect.topleft = (window_w - 1000, 420)
    display_surf.blit(pausebSurf, pausebRect)

    escbSurf = basic_font.render('Выход: Esc', True, info_color)
    escbRect = escbSurf.get_rect()
    escbRect.topleft = (window_w - 1000, 450)
    display_surf.blit(escbSurf, escbRect)


def drawFig(fig, pixelx=None, pixely=None):
    figToDraw = figures[fig['shape']][fig['rotation']]
    if pixelx is None and pixely is None:
        pixelx, pixely = convertCoords(fig['x'], fig['y'])

    # отрисовка элементов фигур
    for x in range(fig_w):
        for y in range(fig_h):
            if figToDraw[y][x] != empty:
                drawBlock(None,
                          None,
                          fig['color'],
                          pixelx + (x * block),
                          pixely + (y * block))


def drawnextFig(fig):  # превью следующей фигуры
    nextSurf = basic_font.render('Следующая:', True, txt_color)
    nextRect = nextSurf.get_rect()
    nextRect.topleft = (window_w - 250, 180)
    display_surf.blit(nextSurf, nextRect)
    drawFig(fig, pixelx=window_w - 200, pixely=230)


if __name__ == '__main__':
    main()
